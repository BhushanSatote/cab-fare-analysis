import pyspark
from pyspark.sql.functions import *
from pyspark.context import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.session import SparkSession
from pyspark.sql import HiveContext
import pyspark.sql.functions as f
sc = SparkContext()
spark= SparkSession.builder.appName("nyc").getOrCreate()
df_parquet1 = spark.read.option("header",True).parquet("s3://mytesting12/abc/cab.parquet/")
df_parquet1.na.drop()
df_parquet2 = df_parquet1.filter("fare_amount > '0'")
df_parquet3 = df_parquet2.withColumn("datetype_timestamp",to_timestamp(col("pickup_datetime"),"yyyy-MM-dd HH:mm:ss"))
df_parquet3 = df_parquet3.withColumn('year',year(df_parquet3.datetype_timestamp))
df_parquet3 = df_parquet3.withColumn('month',month(df_parquet3.datetype_timestamp))
df_parquet3 = df_parquet3.withColumn('day',dayofmonth(df_parquet3.datetype_timestamp))
df_parquet3 = df_parquet3.withColumn('weekdays',dayofweek(df_parquet3.datetype_timestamp))
df_parquet3 = df_parquet3.withColumn('hour',hour(df_parquet3.datetype_timestamp))
df_parquet3.createOrReplaceTempView("nyc3")
sqlContext.sql("Create table nyctaxi select * from nyc3")
